terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.25.0"
    }
  }
}

provider "google" {
  # Configuration options
   project = "bootcamp-420511"
   region = "us-central1"
   credentials = "key.json"
}
