#!/bin/bash

sudo apt-get update 
sudo apt-get upgrade -y

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo apt-get install kubectl -y
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin -y

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

# In Debian, the usermod command directory is not configured in the PATH environment variable.

sudo /usr/sbin/usermod -aG docker gitlab-runner 


sudo gitlab-runner register \
--non-interactive \
--url https://gitlab.com/ \
--token glrt-VxYhgRgZsmj7RSeygsY6 \
--executor shell \